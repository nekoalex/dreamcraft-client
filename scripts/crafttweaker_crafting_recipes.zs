//craftingTable.addShapeless("inscriber_craft1", <item:ae2:calculation_processor_press> , [<item:ae2:fluix_crystal> * 9]);
//craftingTable.addShapeless("inscriber_craft2", <item:ae2:engineering_processor_press> , [<item:ae2:fluix_crystal> * 9]);
//craftingTable.addShapeless("inscriber_craft3", <item:ae2:logic_processor_press> , [<item:ae2:fluix_crystal> * 9]);
//craftingTable.addShapeless("inscriber_craft4", <item:ae2:silicon_press> , [<item:ae2:fluix_crystal> * 9]);
var x = <item:minecraft:air>;
var y = <item:ae2:fluix_crystal>;
var z = <item:minecraft:calcite>;
craftingTable.addShaped("inscriber_craft1", <item:ae2:calculation_processor_press>, [
    [x, y, x],
    [x, y, x],
    [x, y, x]]);

craftingTable.addShaped("inscriber_craft2", <item:ae2:engineering_processor_press>, [
    [y, x, y],
    [x, x, x],
    [y, x, y]]);

craftingTable.addShaped("inscriber_craft3", <item:ae2:logic_processor_press>, [
    [x, y, x],
    [y, y, y],
    [x, y, x]]);

craftingTable.addShaped("inscriber_craft4", <item:ae2:silicon_press>, [
    [y, y, y],
    [y, x, y],
    [y, y, y]]);

craftingTable.addShaped("spectrum_calcite", <item:spectrum:polished_calcite> * 5, [
    [z, x, z],
    [x, z, x],
    [z, x, z]]);


craftingTable.addShaped("creative_item_cell", <item:ae2:creative_item_cell>, [
    [<item:ae2:cell_component_64k>, <item:modern_industrialization:quantum_upgrade>, <item:ae2:cell_component_64k>]]);

craftingTable.addShaped("creative_solar_panel", <item:techreborn:creative_solar_panel>, [
    [<item:techreborn:quantum_solar_panel>, <item:modern_industrialization:quantum_upgrade>]]);

craftingTable.addShapeless("skystone", <item:ae2:sky_stone_block> , [<item:minecraft:stone>, <item:minecraft:coal>, <item:minecraft:furnace>.transformReplace(<item:minecraft:furnace>)]);
craftingTable.addShapeless("questbook", <item:ftbquests:book> , [<item:minecraft:dirt>]);
craftingTable.addShapeless("seed1", <item:bewitchment:aconite_seeds> , [<item:minecraft:wheat_seeds>]);
craftingTable.addShapeless("seed2", <item:bewitchment:belladonna_seeds> , [<item:minecraft:wheat_seeds>,<item:minecraft:wheat_seeds>]);
craftingTable.addShapeless("seed3", <item:bewitchment:mandrake_seeds> , [<item:minecraft:wheat_seeds>,<item:minecraft:wheat_seeds>,<item:minecraft:wheat_seeds>]);
