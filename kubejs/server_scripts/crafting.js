ServerEvents.recipes(event => {
    event.shaped('1x spectrum:quitoxic_reeds', [
      'SAS',
      'S S',
      'SAS'
    ], {
      S: 'minecraft:sugar_cane',
      A: 'spectrum:green_pigment'
    }),
    event.shaped('1x croparia:seed_crop_copper', [
        'SAS',
        'SCS',
        'SAS'
      ], {
        S: 'minecraft:wheat_seeds',
        A: 'minecraft:copper_ingot',
        C: 'croparia:croparia2'
      })
      event.shaped('1x spectrum:mermaids_brush', [
        'SAS',
        'SCS',
        'SAS'
      ], {
        S: 'minecraft:wheat_seeds',
        A: 'spectrum:cyan_pigment',
        C: 'spectrum:topaz_shard'
      })
      event.remove({mod: 'croparia', output: 'croparia:greenhouse'})
      event.remove({mod: 'croparia', output: 'croparia:greenhouse_white'})
      event.remove({mod: 'croparia', output: 'croparia:greenhouse_magenta'})
      event.remove({mod: 'croparia', output: 'croparia:greenhouse_light_blue'})
      event.remove({mod: 'croparia', output: 'croparia:greenhouse_yellow'})
      event.remove({mod: 'croparia', output: 'croparia:greenhouse_lime'})
      event.remove({mod: 'croparia', output: 'croparia:greenhouse_pink'})
      event.remove({mod: 'croparia', output: 'croparia:greenhouse_gray'})
      event.remove({mod: 'croparia', output: 'croparia:greenhouse_light_gray'})
      event.remove({mod: 'croparia', output: 'croparia:greenhouse_cyan'})
      event.remove({mod: 'croparia', output: 'croparia:greenhouse_purple'})
      event.remove({mod: 'croparia', output: 'croparia:greenhouse_orange'})
      event.remove({mod: 'croparia', output: 'croparia:greenhouse_black'})
      event.remove({mod: 'croparia', output: 'croparia:greenhouse_red'})
      event.remove({mod: 'croparia', output: 'croparia:greenhouse_green'})
      event.remove({mod: 'croparia', output: 'croparia:greenhouse_brown'})
      event.remove({mod: 'croparia', output: 'croparia:greenhouse_blue'})
  })